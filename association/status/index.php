<!doctype html>
<html lang="fr">

<head>
    <title>FairSocialNet · La vie privée numérique en Suisse Romande</title>
<?php
$folder_level = 2;
$level = 1;
$path_level = '';
if ($folder_level > 0)
{
	for($level=1;$level<=$folder_level;$level++)
	{  
	$path_level = "$path_level../";
	}
}

include $path_level . 'header.php';
?>
<body class="default">
<?php

include $path_level . 'menu_header.php';
?>
    </header>

    <main>
        <section>
            <div class="services container">
                <div class="row my-5">
                    <div class="col-10 mx-auto text-center">
                        <h2>Statuts (actualisé 05.05.2022)</h2>
                    </div>
                </div>
            </div>
        </section>
    </main>
<?php
$path_level;
include $path_level . 'footer.php';
include $path_level . 'css-js-query.php';
?>

</body>
</html>
